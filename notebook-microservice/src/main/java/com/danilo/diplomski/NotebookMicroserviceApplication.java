package com.danilo.diplomski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NotebookMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotebookMicroserviceApplication.class, args);
	}

}
