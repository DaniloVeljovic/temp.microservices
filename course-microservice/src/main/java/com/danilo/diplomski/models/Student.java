package com.danilo.diplomski.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.danilo.diplomski.models.junctiontables.Registration;

@Entity
@DiscriminatorValue(value = "student")
public class Student extends User {

	public Set<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(Set<Registration> registrations) {
		this.registrations = registrations;
	}

	@OneToMany(mappedBy="student", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	private Set<Registration> registrations;
}
