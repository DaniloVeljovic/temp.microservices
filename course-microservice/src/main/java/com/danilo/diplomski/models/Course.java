package com.danilo.diplomski.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.danilo.diplomski.models.junctiontables.Registration;

@Entity
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;
	@NotNull(message="Name cannot be null")
	private String name;

	private String semester;
	
	private String description;
	
	@OneToMany(mappedBy="courses",fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	private Set<Registration> registratedStudents;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="teacher_id")
	private Teacher teacher;
	
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
	private Set<Obligation> obligations;
	
	public Set<Registration> getRegistratedStudents() {
		return registratedStudents;
	}

	public void setRegistratedStudents(Set<Registration> registratedStudents) {
		this.registratedStudents = registratedStudents;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Set<Obligation> getObligations() {
		return obligations;
	}

	public void setObligations(Set<Obligation> obligations) {
		this.obligations = obligations;
	}
	
	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
