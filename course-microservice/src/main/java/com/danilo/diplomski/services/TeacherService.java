package com.danilo.diplomski.services;

import java.util.Optional;

import com.danilo.diplomski.models.DTO.TeacherDTO;
import com.danilo.diplomski.models.data.Teacher;

public interface TeacherService {

	TeacherDTO createTeacher(TeacherDTO teacher);

	Teacher findByUserID(String teacherID);

}
