package com.danilo.diplomski.controller;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danilo.diplomski.models.DTO.TeacherDTO;
import com.danilo.diplomski.models.UIModels.TeacherRequestModel;
import com.danilo.diplomski.models.UIModels.TeacherResponseModel;
import com.danilo.diplomski.services.TeacherService;


@RestController
@RequestMapping(path="/teachers")
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@PostMapping
	public ResponseEntity<TeacherResponseModel> createTeacher(@RequestBody TeacherRequestModel newTeacher)
	{
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		
		TeacherDTO teacher = modelMapper.map(newTeacher, TeacherDTO.class);
		
		TeacherDTO createdTeacher = teacherService.createTeacher(teacher);
		
		TeacherResponseModel response = modelMapper.map(createdTeacher, TeacherResponseModel.class);
		
		return new ResponseEntity<TeacherResponseModel>(response, HttpStatus.OK);
	}

}
