package com.danilo.diplomski.DTO;

public class RegistrationDTOResponse {

	
	private Integer idStudent;
	private Integer idCourse;
	public Integer getIdStudent() {
		return idStudent;
	}
	public void setIdStudent(Integer idStudent) {
		this.idStudent = idStudent;
	}
	public Integer getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(Integer idCourse) {
		this.idCourse = idCourse;
	}
	
	
}
