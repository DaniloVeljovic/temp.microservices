package com.danilo.diplomski.DTO;

import javax.validation.constraints.NotNull;

public class CourseDTORequest {
	
	@NotNull
	private String name;
	
	@NotNull
	private String semester;
	
	@NotNull
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
