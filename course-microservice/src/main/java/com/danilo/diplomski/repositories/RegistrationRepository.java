package com.danilo.diplomski.repositories;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.danilo.diplomski.models.data.junctiontables.Registration;

@Transactional
public interface RegistrationRepository extends CrudRepository<Registration, Integer> {

	Iterable<Registration> findByStudentId(Integer id);
}
