package com.danilo.diplomski.courseservices.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.danilo.diplomski.DTO.CourseDTOResponse;
import com.danilo.diplomski.DTO.RegistrationDTOResponse;
import com.danilo.diplomski.courseservices.CourseService;
import com.danilo.diplomski.models.Course;
import com.danilo.diplomski.models.Teacher;
import com.danilo.diplomski.models.junctiontables.Registration;

@Service
public class CourseServiceImpl implements CourseService {

	@Override
	public Course createCourse(Course newCourse, Teacher teacher) throws IllegalArgumentException{
		newCourse.setTeacher(teacher);
		return newCourse;
	}

	@Override
	public CourseDTOResponse convertToDTOResponse(Course course) throws IllegalArgumentException{
	
		CourseDTOResponse returnVal = new CourseDTOResponse();
		returnVal.setDescription(course.getDescription());
		returnVal.setId(course.getId());
		returnVal.setName(course.getSemester());
		returnVal.setSemester(course.getSemester());
		return returnVal;
	}

	@Override
	public List<CourseDTOResponse> convertToIterableDTOResponses(Iterable<Course> foundCourses) {
		List<CourseDTOResponse> returnVal = new ArrayList<CourseDTOResponse>();
		
		for(Course course : foundCourses)
		{
			CourseDTOResponse help = new CourseDTOResponse();
			help.setDescription(course.getDescription());
			help.setId(course.getId());
			help.setName(course.getSemester());
			help.setSemester(course.getSemester());
			
			returnVal.add(help);			
		}
		
		return returnVal;
	}

	@Override
	public Course updateCourse(Course foundCourse, String desc) {
		// TODO Auto-generated method stub
		foundCourse.setDescription(desc);
		return foundCourse;
	}

	@Override
	public RegistrationDTOResponse convertToRegistrationDTOResponse(Registration savedReg) {
	
		RegistrationDTOResponse returnVal = new RegistrationDTOResponse();
		
		returnVal.setIdCourse(savedReg.getStudent().getOriginalId());
		returnVal.setIdCourse(savedReg.getCourses().getId());
		
		
		
		return returnVal;
	}

}
