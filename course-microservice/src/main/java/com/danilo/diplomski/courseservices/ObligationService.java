package com.danilo.diplomski.courseservices;

import com.danilo.diplomski.DTO.ObligationDTO;
import com.danilo.diplomski.models.Course;
import com.danilo.diplomski.models.Obligation;

public interface ObligationService {

	Obligation addCourseToObligation(Obligation oblig, Course course);

	ObligationDTO convertToResponseDTO(Obligation saved);
}
