package com.danilo.diplomski.courseservices;

import java.util.List;

import com.danilo.diplomski.DTO.CourseDTOResponse;
import com.danilo.diplomski.DTO.RegistrationDTOResponse;
import com.danilo.diplomski.models.Course;
import com.danilo.diplomski.models.Teacher;
import com.danilo.diplomski.models.junctiontables.Registration;

public interface CourseService {

	Course createCourse(Course newCourse, Teacher teacher) throws IllegalArgumentException;
	
	CourseDTOResponse convertToDTOResponse(Course course) throws IllegalArgumentException;
	
	List<CourseDTOResponse> convertToIterableDTOResponses (Iterable<Course> foundCourses);

	Course updateCourse(Course foundCourse, String desc);

	RegistrationDTOResponse convertToRegistrationDTOResponse(Registration savedReg);


}
