package com.danilo.diplomski.courseservices.impl;

import org.springframework.stereotype.Service;

import com.danilo.diplomski.DTO.ObligationDTO;
import com.danilo.diplomski.courseservices.ObligationService;
import com.danilo.diplomski.models.Course;
import com.danilo.diplomski.models.Obligation;

@Service
public class ObligationServiceImpl implements ObligationService {

	@Override
	public Obligation addCourseToObligation(Obligation oblig, Course course) {
		oblig.setCourses(course);
		return oblig;
	}

	@Override
	public ObligationDTO convertToResponseDTO(Obligation saved) {
		
		ObligationDTO returnVal = new ObligationDTO();
		
		returnVal.setDate(saved.getDate());
		returnVal.setDescription(saved.getDescription());
		returnVal.setId(saved.getId());
		returnVal.setType(saved.getType());
		
		return returnVal;
	}

}
